# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table `comment` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`content` VARCHAR(254) NOT NULL,`video_id` VARCHAR(254) NOT NULL,`user_uuid` VARCHAR(254) NOT NULL);
create table `favorite` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`video_id` VARCHAR(254) NOT NULL,`user_uuid` VARCHAR(254) NOT NULL,`valid` BOOLEAN DEFAULT true NOT NULL);
create table `play` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`video_id` VARCHAR(254) NOT NULL,`user_uuid` VARCHAR(254) NOT NULL);
create table `user` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`uuid` VARCHAR(254) NOT NULL,`profile_pic` VARCHAR(254) NOT NULL,`name` VARCHAR(254) NOT NULL,`valid` BOOLEAN DEFAULT true NOT NULL);
create unique index `UUID_IDX` on `user` (`uuid`);

# --- !Downs

drop table `user`;
drop table `play`;
drop table `favorite`;
drop table `comment`;

