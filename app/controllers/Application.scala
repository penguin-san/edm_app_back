package controllers

import play.api._
import play.api.db.slick._
import play.api.mvc._
import play.api.Play.current
import models._
import scala.slick.driver.MySQLDriver.simple._

object Application extends Controller {

  def index = Action { request => 
      val req = request.queryString
      DB.withSession { implicit sn =>
        Users.getUser("123") match {
            case Some(s) => println("user existed")
            case None => Users.create("", "123879", "default", true)
        }
        Ok("sample")
      }
  }

}
