package controllers

import play.api._
import play.api.db.slick._
import play.api.mvc._
import play.api.Play.current
import models._
import scala.slick.driver.MySQLDriver.simple._


object User extends Controller {
    def index = Action { request =>
        val uuid = request.queryString("uuid")(0)
        DB.withSession { implicit sn =>
            Users.getUser(uuid) match {
                case Some(s) => println("user exsited")
                case None => Users.create("", uuid, "default", true)
            }
        }
        Ok("sample")
    }    
}
