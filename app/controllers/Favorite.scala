package controllers

import play.api._
import play.api.db.slick._
import play.api.mvc._
import play.api.Play.current
import models._
import scala.slick.driver.MySQLDriver.simple._

object Favorite extends Controller {
    def create = Action { request => 
        val video_id = request.queryString("video_id")(0)
        val uuid = request.queryString("uuid")(0)
        DB.withSession { implicit sn => 
            Favorites.find( video_id, uuid) match {
                case Some(s) => Favorites.update( video_id, uuid, true)
                case None => Favorites.favo( video_id, uuid, true)
            }
        }
        Ok("sample")
    }

    def delete = Action { request =>
        DB.withSession { implicit sn => 
            val video_id = request.queryString("video_id")(0)
            val uuid = request.queryString("uuid")(0)
            DB.withSession { implicit sn => 
                Favorites.find( video_id, uuid) match {
                    case Some(s) => Favorites.update( video_id, uuid, false)
                    case None => Ok("invalid order") 
                }
            }
        }
        Ok("delete complete")
    }

    def index = Action {request =>
        import play.api.libs.json._
        val uuid = request.queryString("uuid")(0)
        DB.withSession { implicit sn =>
            val result = Favorites.findByUUID("123456")
            Ok("sample")
        }
    }
}
 
