
package controllers

import play.api._
import play.api.db.slick._
import play.api.mvc._
import play.api.Play.current
import models._
import scala.slick.driver.MySQLDriver.simple._

object Comment extends Controller {
    def create = Action { request =>
        val video_id = request.queryString("video_id")(0)
        val user_id = request.queryString("user_uuid")(0)
        val content = request.queryString("content")(0)

        DB.withSession { implicit sn =>
            Comments.create(content, video_id, user_id)
            Ok("success")
        }
    }    
    def findByVideoId = Action { request =>
        import play.api.libs.json._
        val video_id = request.queryString("video_id")(0)
        DB.withSession { implicit sn =>
            val json = Json.toJson(
                Comments.findByVideoId(video_id).map { t =>
                    Map("content" ->t._1, "video_id" -> t._2, "user_uuid" -> t._3)
                })
            Ok(json)
        }
    }
}
