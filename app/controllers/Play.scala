package controllers

import play.api._
import play.api.db.slick._
import play.api.mvc._
import play.api.Play.current
import models._
import scala.slick.driver.MySQLDriver.simple._


object Play extends Controller {
    def create = Action { request =>
        val video_id = request.queryString("video_id")(0)
        val uuid = request.queryString("uuid")(0)
        println(uuid)
        println(video_id)
        DB.withSession { implicit sn =>
            Plays.create( uuid, video_id) 
        }
        Ok("play")
    }
}
 
