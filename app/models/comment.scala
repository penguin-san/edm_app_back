
package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.libs.json._

case class Comment( id: Option[Long], content: String, video_id: String, user_uuid: String )
class Comments(tag: Tag) extends Table[(Long, String, String, String)](tag, "comment") {
    
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def content = column[String]("content")
    def video_id = column[String]("video_id", O.NotNull)
    def user_uuid = column[String]("user_uuid", O.NotNull)

    def * = (id, content, video_id, user_uuid)
}

object Comments extends DAO {
    def create( content: String, video_id:String, user_uuid: String)(implicit s: Session) {
        Comments map {
            r => (r.content, r.video_id, r.user_uuid)
        } += ( content, video_id, user_uuid)
    }

    def findByVideoId( video_id: String )(implicit s:Session):List[(String, String, String)] = {

        Comments.filter {
            _.video_id === video_id
        } map {
            r => (r.content, r.video_id, r.user_uuid)
        } list

    }

}
