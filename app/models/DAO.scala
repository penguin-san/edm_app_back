
package models

import scala.slick.lifted.TableQuery


private[models] trait DAO {
    val Users = TableQuery[Users]   
    val Favorites = TableQuery[Favorites]
    val Plays = TableQuery[Plays]
    val Comments = TableQuery[Comments]
}
