
package models

import scala.slick.driver.MySQLDriver.simple._

case class Play( id: Option[Long], video_id: String, user_uuid: String )
class Plays(tag: Tag) extends Table[(Long, String, String)](tag, "play") {
    
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def video_id = column[String]("video_id", O.NotNull)
    def user_uuid = column[String]("user_uuid", O.NotNull)

    def * = (id, video_id, user_uuid)
}

object Plays extends DAO {
    def create(uuid: String, video_id: String)(implicit s: Session) = {
        Plays map {
            r => ( r.video_id, r.user_uuid)
        } += ( video_id, uuid)
    }
}
