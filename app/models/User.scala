
package models

//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._

case class User( id: Option[Long], uuid:String, profile_pic: String, name: String, valid: Boolean )

class Users(tag: Tag) extends Table[(Long, String, String, String, Boolean)](tag, "user") {
    def id   = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def uuid = column[String]("uuid", O.NotNull)
    def name = column[String]("name")
    def profile_pic = column[String]("profile_pic")
    def valid = column[Boolean]("valid", O.Default(true))

    def uuidIndex = index("UUID_IDX", uuid, unique = true)

    def *    = (id, uuid, profile_pic, name, valid) 
}

object Users extends DAO {
    def getUser(uuid: String)(implicit s: Session) = {
        Users.filter {
            _.uuid === uuid
        } map {
            _.uuid
        } firstOption
    }

    def create(name: String, uuid: String, profile_icon: String, valid: Boolean)(implicit s: Session) = {
        Users map {
            r => (r.name, r.uuid, r.profile_pic, r.valid)
        } += (name, uuid, profile_icon, valid)
    }
}
