
package models

import scala.slick.driver.MySQLDriver.simple._

case class Favorite( id: Option[Long], video_id: String, user_uuid: String, valid: Boolean)
class Favorites(tag: Tag) extends Table[(Long, String, String, Boolean)](tag, "favorite") {
    
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def video_id = column[String]("video_id", O.NotNull)
    def user_uuid = column[String]("user_uuid", O.NotNull)
    def valid = column[Boolean]("valid", O.Default(true))

    def * = (id, video_id, user_uuid, valid)
}

object Favorites extends DAO {
    def favo(video: String, user_uuid: String, valid: Boolean)(implicit s: Session) = {
        Favorites map {
            r => (r.video_id, r.user_uuid, r.valid)
        } += (video, user_uuid, valid)
    }

    def find(videoId: String, user_uuid: String)(implicit s: Session) = {
        Favorites filter {
            r => (r.video_id === videoId) && (r.user_uuid === user_uuid)
        } firstOption
    }

    def update(video: String, user_uuid: String, valid: Boolean)(implicit s: Session) = {
        Favorites filter {
            r => (r.video_id === video) && (r.user_uuid === user_uuid)
        } map {
            _.valid
        } update(valid)
   }
   def findByUUID(user_uuid: String)(implicit s: Session): List[( String, String, Boolean)]  = {
        Favorites filter {
            _.user_uuid === user_uuid
        } map {
            r => (r.video_id, r.user_uuid, r.valid)
        } list
   }

}
